<?php

/**
 * Implements hook_variable_info().
 */
function deeptabs_destination_variable_info() {
  $variables['deeptabs_destination_patterns'] = array(
    'type' => 'text',
    'title' => t('Path patterns for Deep Tabs Destination'),
    'description' => t('Patterns, each on one line. Use * for multi-level wildcard.'),
    'default' => '*',
  );
  $variables['deeptabs_destination_keywords'] = array(
    'type' => 'text',
    'title' => t('Keywords for Deep Tabs Destination'),
    'description' => t('Patterns, each on one line. Use * for multi-level wildcard.'),
    'default' => 'destination',
  );
  return $variables;
}
